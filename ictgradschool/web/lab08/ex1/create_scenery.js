$(document).ready(function () {
    // TODO: Your code here

    var vert=0;
    var scale=0;
    var horiz=0;
    for (let i = 1; i < 7; i++) {
        $("#radio-background" + i).change(function () {
            let ext = '.jpg';
            if (i == 6) {
                ext = '.gif';
            }
            $("#background").attr('src', '../images/background' + i + ext);
        });
    }


    for (let i = 1; i < 9; i++) {
        $("#check-dolphin" + i).change(function () {
            if ($(this).is(':checked')) {

                $('#dolphin' + i).show();
            } else {
                $('#dolphin' + i).hide();
            }


        }).change();
    }



    function change() {
        $('.dolphin').css('transform','translate('+horiz+'px, ' + vert +'px) ' + 'scale('+scale+') ');
    }


    $('#vertical-control').on('input',function () {
            vert = $(this).val();
            change()
            //$('.dolphin').css('transform','translateY('+$(this).val()+'px) ');

    })


    $('#horizontal-control').on('input',function () {
        horiz = $(this).val()
        change()
        //$('.dolphin').css('transform','translateX('+$(this).val()+'px) ');

    })

    $('#size-control').on('input',function () {
        scale = (1+($(this).val()/100))
        change()
        //$('.dolphin').css('transform','scale('+(1+($(this).val()/100))+') ');

    })



});
